package com.example.projectvg.views;

import com.example.projectvg.components.UserForm;
import com.example.projectvg.security.PrincipalUtility;
import com.example.projectvg.services.AppUserService;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import javax.annotation.security.PermitAll;

@Route(value = "/userdetails", layout = AppView.class)
@PageTitle("User Details")
@PermitAll
public class UserDetailsView extends VerticalLayout {

    private UserForm userForm;
    private AppUserService appUserService;

    public UserDetailsView(AppUserService appUserService) {
        this.appUserService = appUserService;

        this.userForm = new UserForm(appUserService, this);

        userForm.setAppUser(
                appUserService.findAppUserByUsername(PrincipalUtility.getPrincipalName())
        );
        add(userForm);

    }

    public void updateView() {




    }
}
