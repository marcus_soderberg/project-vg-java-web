package com.example.projectvg.views;

import com.example.projectvg.security.PrincipalUtility;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;

@CssImport("./my-styles/styles.css")
@Route(value = "/login", layout = AppView.class)
public class LoginView extends Composite<VerticalLayout> implements BeforeEnterObserver {

    LoginOverlay loginOverlay;
    public LoginView() {

        this.getContent().addClassName("login-container");

        H1 title = new H1("Välkommen");
        title.addClassName("header-title");

        Button button = new Button("Logga in", event -> {
            loginOverlay = new LoginOverlay();
            loginOverlay.setTitle("Todo");
            loginOverlay.setDescription("Få saker gjort");
            loginOverlay.setOpened(true);
            loginOverlay.setAction("login");
        });

        if (PrincipalUtility.isLoggedIn()){
            button.setVisible(false);
        }

        button.addClassName("login-button");
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_CONTRAST);

        VerticalLayout welcomeLayout = new VerticalLayout();
        welcomeLayout.addClassName("welcome-layout");
        welcomeLayout.setSizeFull();
        welcomeLayout.add(title, button);
        welcomeLayout.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.CENTER);
        welcomeLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.CENTER);

        this.getContent().add(welcomeLayout);


    }

    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        boolean loginFailed = beforeEnterEvent
                .getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error");

        if (loginFailed) {
            loginOverlay.setError(true);
        }
    }
}
