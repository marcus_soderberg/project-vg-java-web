package com.example.projectvg.views;

import com.example.projectvg.security.PrincipalUtility;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLink;

public class AppView extends AppLayout {

    public AppView() {

        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setHeight("50px");

        DrawerToggle drawerToggle = new DrawerToggle();
        drawerToggle.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        Button logoutButton = new Button("Logga ut", event -> PrincipalUtility.logout());
        logoutButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        headerLayout.add(
                drawerToggle,
                new H1("Planera din dag")
        );
        setHeaderStyle(headerLayout);
        addToNavbar(headerLayout);

        RouterLink userDetailsLink = new RouterLink("Kontodetaljer", UserDetailsView.class);
        RouterLink todoLink = new RouterLink("Dina Todo's", TodoView.class);


        VerticalLayout sideDrawerContent = new VerticalLayout(userDetailsLink, todoLink);

        addToDrawer(sideDrawerContent);

        if (!PrincipalUtility.isLoggedIn()) {
            drawerToggle.setVisible(false);
            headerLayout.setVisible(false);

        } else {
            drawerToggle.setVisible(true);
            headerLayout.add(logoutButton);
        }
        setDrawerOpened(false);

    }

    public void setHeaderStyle(HorizontalLayout headerLayout) {
        headerLayout.setWidthFull();
        headerLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        headerLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        headerLayout.setMargin(true);
    }

}
