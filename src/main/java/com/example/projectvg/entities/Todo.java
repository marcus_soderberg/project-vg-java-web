package com.example.projectvg.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    @NotBlank
    private String activity;

    @Column
    private int priority;

    @Column
    private boolean isCompleted;

    @ManyToOne
    @JoinColumn(name = "appuser_id")
    private AppUser appUser;

    public Todo(String activity, int priority, boolean isCompleted, AppUser appUser) {
        this.activity = activity;
        this.priority = priority;
        this.isCompleted = isCompleted;
        this.appUser = appUser;
    }

    public Todo() {

    }

    public int getId() {
        return id;
    }

    public String getActivity() {
        return activity;
    }

    public int getPriority() {
        return priority;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
}
