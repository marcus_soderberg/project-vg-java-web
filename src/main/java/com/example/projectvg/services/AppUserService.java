package com.example.projectvg.services;

import com.example.projectvg.entities.AppUser;
import com.example.projectvg.repositories.AppUserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AppUserService {

    AppUserRepository appUserRepository;

    public AppUserService(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public AppUser findAppUserByUsername(String username) {
        return appUserRepository.findAppUserByUsername(username).orElseThrow();
    }

    public AppUser updateById(AppUser appUser, int id){
        AppUser existingAppUser = appUserRepository.findById(id).orElseThrow();
        existingAppUser.setName(appUser.getName());
        existingAppUser.setEmail(appUser.getEmail());
        return appUserRepository.save(existingAppUser);
    }

}
