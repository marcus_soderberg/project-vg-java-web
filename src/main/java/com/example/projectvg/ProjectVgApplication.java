package com.example.projectvg;

import com.example.projectvg.entities.AppUser;
import com.example.projectvg.entities.Todo;
import com.example.projectvg.repositories.AppUserRepository;
import com.example.projectvg.repositories.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class ProjectVgApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectVgApplication.class, args);
    }

    @Bean
    CommandLineRunner init(TodoRepository todoRepository, AppUserRepository appUserRepository){
        return args -> {
//            AppUser appUser1 = new AppUser("mackan", "coolt!", "Marcus Söderberg", "marcus_soderberg@outlook.com");
//            AppUser appUser2 = new AppUser("robban", "svinc00lt!","Robin Bloom", "hagrid@robban.net");
//            appUserRepository.saveAll(List.of(appUser1, appUser2));
//
//            Todo todo1 = new Todo("Äta mat", 3, false, appUser1);
//            Todo todo2 = new Todo("Dricka kaffe", 2, true, appUser2);
//            Todo todo3 = new Todo("Spela spel?", 1, false, appUser1);
//            todoRepository.save(todo1);
//            todoRepository.save(todo2);
//            todoRepository.save(todo3);
        };
    }


}
