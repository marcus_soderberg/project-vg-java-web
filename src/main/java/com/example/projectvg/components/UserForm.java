package com.example.projectvg.components;

import com.example.projectvg.entities.AppUser;
import com.example.projectvg.entities.Todo;
import com.example.projectvg.services.AppUserService;
import com.example.projectvg.views.UserDetailsView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;

public class UserForm extends FormLayout {

    TextField username = new TextField("Användarnamn");
    TextField name = new TextField("Namn");
    TextField email = new TextField("Email");

    Button saveButton = new Button("Spara");

    Binder<AppUser> binder = new BeanValidationBinder<>(AppUser.class);

    AppUserService appUserService;
    UserDetailsView userDetailsView;

    public UserForm(AppUserService appUserService, UserDetailsView userDetailsView) {

        this.appUserService = appUserService;
        this.userDetailsView = userDetailsView;
        binder.bindInstanceFields(this);

        saveButton.addClickListener(event -> saveAppUser());

        username.setWidth("500px");
        username.setReadOnly(true);
        name.setWidth("500px");
        email.setWidth("500px");


        VerticalLayout verticalLayout = new VerticalLayout(username, name, email, saveButton);


        add(verticalLayout);

    }

    public void setAppUser(AppUser appUser){
        binder.setBean(appUser);
    }

    private void saveAppUser() {

        AppUser appUser = binder.validate().getBinder().getBean();
        appUserService.updateById(appUser, appUser.getId());
        Notification.show("Sparat").addThemeVariants(NotificationVariant.LUMO_SUCCESS);
    }
}
