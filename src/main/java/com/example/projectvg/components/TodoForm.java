package com.example.projectvg.components;

import com.example.projectvg.entities.Todo;
import com.example.projectvg.services.TodoService;
import com.example.projectvg.views.TodoView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;

public class TodoForm extends FormLayout {

    TextField activity = new TextField("Aktivitet");
    ComboBox<Integer> priority = new ComboBox<>("Prio");
    Button saveButton = new Button("Spara");

    Binder<Todo> binder = new BeanValidationBinder<>(Todo.class);

    TodoService todoService;
    TodoView todoView;

    public TodoForm(TodoService todoService, TodoView todoView) {
        this.todoService = todoService;
        this.todoView = todoView;
        binder.bindInstanceFields(this);

        saveButton.addClickListener(event -> saveTodo());

        priority.setAllowCustomValue(true);
        priority.setItems(1, 2, 3);
        activity.setPlaceholder("Ange aktivitet...");
        activity.setMaxLength(105);

        add(activity, priority, saveButton);
    }

    public void setTodo(Todo todo) {
        binder.setBean(todo);
    }

    public void saveTodo() {
        Todo todo = binder.validate().getBinder().getBean();
        if (todo.getId() == 0) {
            todoService.save(todo);
        } else {
            todoService.updateById(todo.getId(), todo);
        }
        setTodo(null);
        todoView.updateView();

        this.getParent().ifPresent(component -> {
            if(component instanceof Dialog){
                ((Dialog) component).close();
            }
        });
    }

}
